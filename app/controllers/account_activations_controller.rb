class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      log_in user
      flash[:success] = "本登録が完了しました。"
      redirect_to user_page_url(user)
    else
      flash[:danger] = "すでに本登録が完了している、または本登録用リンクが無効です。"
      redirect_to root_url
    end
  end
end
